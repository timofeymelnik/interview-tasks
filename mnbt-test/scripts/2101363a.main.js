!function ($) {
    'use strict';
    var slider = function (element, settings) {
        var self = this;

        this.$el = element;
        this.current = 0;
        this.settings = settings;
        this.wrap = this.$el.css('overflow', 'hidden').children('ul');
        this.container = this.wrap.children('li');
        this.containerWidth = this.container.first().outerWidth();
        this.containerLen = this.container.length;
        this.nav = this.$el.find('nav');
        this.createNavigation();
        if ('fade' === this.settings.animation) {
            this.container.first().addClass('active')
        } else {
            this.wrap.css({width: this.containerLen * this.containerWidth + 'px'});
        }
        this.navcontrol = this.nav.find(this.settings.navcontrol);
        this.nav.on('click', '[data-slide]', function (event) {
            self.setPosition($(this).data('slide'), event)
        });
    };
    slider.defaults = {
        navcontrol: 'a',
        navcontrols: true,
        navcontrolstpl: '<a data-slide="{{i}}"></a>',
        navdirection: true,
        prevtext: 'Prev',
        nexttext: 'Next',
        animation: 'slide',
        items: 1,
        animationTime: 300,
        slide: function (){}
    };
    slider.prototype.createNavigation = function () {
        var itemsLength = this.containerLen / this.settings.items;
        if (itemsLength > 1) {
            if (this.settings.navcontrols) {
                for (var controls = '', d = 1; itemsLength + 1 > d; d++) controls += this.settings.navcontrolstpl.replace(/{{i}}/g, d);
                $(this.nav, this.wrap).append(controls).find('a', this.nav).eq(this.current).addClass('active')
            }
            this.settings.navdirection && $(this.nav, this.wrap).prepend('<button data-slide="prev">{{prevtext}}</button>'.replace(/{{prevtext}}/, this.settings.prevtext)).append('<button data-slide="next">{{nexttext}}</button>'.replace(/{{nexttext}}/, this.settings.nexttext))
        }
    };
    slider.prototype.animate = function (event) {
        var width = - (this.current * this.containerWidth * this.settings.items);
        if ('slide' === this.settings.animation) {
            if (Modernizr.csstransitions) {
                this.wrap.css({transition: 'all ' + 0.001 * this.settings.animationTime + 's linear', transform: 'translate(' + width + 'px, 0)'})
            } else {
                this.wrap.stop(true, false).animate({'margin-left': width}, this.settings.animationTime);
            }
        } else {
            this.container.removeClass('active').eq(this.current).addClass('active');
        }
        this.navcontrol.removeClass('active').eq(this.current).addClass('active');
        this.settings.slide.call(this, event);
    };
    slider.prototype.setPosition = function (index, event) {
        var current = this.current,
            delta = this.containerLen / this.settings.items;
        current += ~~('next' === index) || - 1;
        if (typeof index == 'string') {
            this.current = (0 > current) ? delta - 1 : current % delta;
        } else {
            'number' == typeof index && (this.current = index - 1);
        }
        this.animate(event);
    };
    $.fn.slider = function (config) {
        return this.each(function () {
            if (!$.data(this, 'plugin_slider')) {
                {
                    var element = $(this),
                        configExtended = $.extend({}, slider.defaults, config, $(this).data());
                    new slider(element, configExtended)
                }
                $.data(this, 'plugin_slider')
            }
        });
    };
    $.fn.slider.Constructor = slider
}(jQuery);
$(function () {
    'use strict';
    function a(a, b, c) {
        var d = 'left' === c ? a[0].clientWidth : a[0].clientHeight,
            e = 'left' === c ? b[0].clientWidth : b[0].clientHeight,
            f = 'left' === c ? a[0].offsetLeft : a[0].offsetTop,
            g = f + d / 2 - e / 2;
        Modernizr.csstransitions ? b.css(c, g)  : b.stop(true, false).animate(c, g)
    }
    function b(a, b) {
        var c = {
                text: /^[\w]+$/,
                email: /^[\.\w_\-\+]+[@][\w_\-]+([.][\w_\-]+)+([\A-z]{1,4})$/,
                phone: /^\d+$/,
                password: /^\w+$/
            },
            d = /(email|password|phone|card)/.exec(a.type) || b,
            e = d ? d[0] : 'text',
            f = c[e].test(a.value) ? true : false;
        return f
    }
    $('nav.navbar').each(function () {
        var b = $(this),
            c = b.find('li'),
            d = b.find('.triangle');
        c.on('mouseenter', function () {
            a($(this), d, 'left')
        }),
            a(c.first(), d, 'left')
    }),
        $('.nav-tabs a').on('click', function (a) {
            a.preventDefault(),
                $(this).tab('show')
        }),
        $('.slider').slider({
            items: 3,
            navdirection: false
        }),
        $('.featured-slider').slider({
            navcontrolstpl: '<a data-slide="{{i}}">{{i}}</a>',
            prevtext: '<img src="images/arrow-left.png">',
            nexttext: '<img src="images/arrow-right.png">'
        }),
        $('.vertical-slider').each(function () {
            var b = $(this),
                c = b.find('nav li'),
                d = b.find('.triangle');
            c.each(function () {
                $(this).attr('data-slide', arguments[0] + 1)
            }),
                a(c.first(), d, 'top'),
                b.slider({
                    animation: 'fade',
                    navcontrols: false,
                    navdirection: false,
                    navcontrol: 'li',
                    slide: function (b) {
                        a($(b.currentTarget), d, 'top')
                    }
                })
        }),
        $('form').each(function () {
            function a() {
                g.on('focusout', d),
                    g.on('focusin', c),
                    f.on('submit', e)
            }
            function c(a) {
                $(a.target).parent().addClass('focus').removeClass('has-success, has-error')
            }
            function d(a) {
                var c = $(a.target).parent();
                if ('' === $(a.target).val()) c.removeClass('focus').addClass('has-error');
                else {
                    var d = b(a.target) ? 'has-success' : 'has-error';
                    c.addClass(d)
                }
            }
            function e(a) {
                a.preventDefault();
                for (var c = 0; c < g.length; c++) {
                    var d = b(g[c]) ? 'has-success' : 'has-error';
                    g.parent().addClass(d)
                }
            }
            var f = $(this),
                g = f.find('input');
            a()
        })
});
