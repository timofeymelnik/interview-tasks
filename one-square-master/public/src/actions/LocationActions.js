/**
 * Created by timofeymelnik on 11/2/16.
 */

import AppDispatcher from "../dispatcher/AppDispatcher.js";
import LocationConstants from "../constants/LocationConstants.js"

export default {
    setLocation(location) {
        AppDispatcher.dispatch({
            actionType: LocationConstants.SET_LOCATION,
            location: location
        })
    },
    removeLocation() {
        AppDispatcher.dispatch({
            actionType: LocationConstants.REMOVE_LOCATION
        })
    }
}