/**
 * Created by timofeymelnik on 10/31/16.
 */
"use strict";

import http from '../api.js';

import AppDispatcher from "../dispatcher/AppDispatcher";
import usersConstants from "../constants/UserConstants";
import AuthStore from "../stores/AuthStore"

export default {
    getUsers: () => {
        http('/api/v1/users')
            .get()
            .then(users => {
                AppDispatcher.dispatch({
                    actionType: usersConstants.GET_USERS,
                    data: users
                });
            })
            .catch(err => {
                AppDispatcher.dispatch({
                    actionType: usersConstants.GET_USERS_ERROR,
                    message: err
                });
            })
    },
    getUser: (id) => {
        http(`/api/v1/users/${id}`)
            .get(null, {
                headers: {
                    'x-access-token': AuthStore.getJWT()
                }
            })
            .then(user => {
                AppDispatcher.dispatch({
                    actionType: usersConstants.GET_USER,
                    data: user
                });
            })
            .catch(err => {
                AppDispatcher.dispatch({
                    actionType: usersConstants.GET_USER_ERROR,
                    message: err
                });
            })
    },
    updateUser: (id, data) => {
        http(`/api/v1/users/${id}`)
            .put(data, {
                headers: {
                    'x-access-token': AuthStore.getJWT()
                }
            })
            .then(user => {
                AppDispatcher.dispatch({
                    actionType: usersConstants.UPDATE_USER,
                    data: user
                });
            })
    },
    addLocation: (id, data) => {
        http(`/api/v1/users/${id}/locations`)
            .post(data, {
                headers: {
                    'x-access-token': AuthStore.getJWT()
                }
            })
            .then(user => {
                AppDispatcher.dispatch({
                    actionType: usersConstants.ADD_LOCATION,
                    data: user
                });
            })
    }
}