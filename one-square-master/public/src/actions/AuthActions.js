/**
 * Created by timofeymelnik on 11/1/16.
 */

import AppDispatcher from "../dispatcher/AppDispatcher";
import AuthConstants from "../constants/AuthConstants";
import http from "../api";

export default {
    auth: (credentials, token) => {
        return http('/api/v1/auth')
            .post(credentials, {
                headers: {
                    'x-access-token': token
                }
            })
            .then(response => {
                AppDispatcher.dispatch({
                    actionType: AuthConstants.AUTH_USER,
                    credentials: credentials,
                    token: response.token
                });
            });

    },
    logOut: () => {
        AppDispatcher.dispatch({
            actionType: AuthConstants.LOGOUT_USER
        })
    }
}

