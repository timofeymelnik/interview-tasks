/**
 * Created by timofeymelnik on 11/2/16.
 */

import AppDispatcher from '../dispatcher/AppDispatcher';
import MarkerConstants from '../constants/MarkerConstants';

export default {
    setMarker(marker) {
        AppDispatcher.dispatch({
            actionType: MarkerConstants.SET_MARKER,
            marker: marker
        })
    },
    removeMarker() {
        AppDispatcher.dispatch({
            actionType: MarkerConstants.REMOVE_MARKER,
        })
    }
}