/**
 * Created by timofeymelnik on 11/1/16.
 */

import AppDispatcher from '../dispatcher/AppDispatcher';
import MapTypeConstants from '../constants/MapTypeConstants';

export default {
    setType(type) {
        AppDispatcher.dispatch({
            actionType: MapTypeConstants.SET_TYPE,
            type: type
        })
    }
}