/**
 * Created by timofeymelnik on 11/2/16.
 */

export default {
    SET_LOCATION: 'SET_LOCATION',
    REMOVE_LOCATION: 'REMOVE_LOCATION'
}