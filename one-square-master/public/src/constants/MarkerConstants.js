/**
 * Created by timofeymelnik on 11/2/16.
 */

export default {
    SET_MARKER: 'SET_MARKER',
    REMOVE_MARKER: 'REMOVE_MARKER'
};