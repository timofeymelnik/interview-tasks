/**
 * Created by timofeymelnik on 11/2/16.
 */

export default {
    SET_TYPE: 'SET_TYPE',
    OPTIONS: {
        pharmacy: 'pharmacy',
        restaurant: 'restaurant',
        gas_station: 'gas station',
        school: 'school'
    }
}