/**
 * Created by timofeymelnik on 11/1/16.
 */

export default {
    AUTH_USER: 'AUTH_USER',
    LOGOUT_USER: 'LOGOUT_USER'
}