/**
 * Created by timofeymelnik on 10/31/16.
 */

export default {
    GET_USER: 'GET_USER',
    UPDATE_USER: 'UPDATE_USER',
    ADD_LOCATION: 'ADD_LOCATION',
    GET_USERS: 'GET_USERS',
    GET_USER_ERROR: 'GET_USER_ERROR',
    GET_USERS_ERROR: 'GET_USERS_ERROR'
}