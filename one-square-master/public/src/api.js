/**
 * Created by timofeymelnik on 10/31/16.
 */

export default function(url) {

    function generateUri(args) {
        let uri = '?';
        var argCount = 0;
        for (let key in args) {
            if (args.hasOwnProperty(key)) {
                if (argCount++) {
                    uri += '&';
                }
                uri += `${encodeURIComponent(key)}=${encodeURIComponent(args[key])}`;
            }
        }
    }

    function ajax(method, url, args, opts) {

        // Return the promise
        return new Promise(function(resolve, reject) {

            // Instantiates the XMLHttpRequest
            var client = new XMLHttpRequest();
            var uri = url;
            var data;

            if (args) {
                if (method === 'GET') {
                    client.open(method, `${uri}${generateUri(args)}`);
                } else {
                    client.open(method, uri);
                    client.setRequestHeader('Content-Type', 'application/json');
                    data = JSON.stringify(args)
                }
            } else {
                client.open(method, url);
            }

            if (opts && opts.headers) {
                let headers = opts.headers;
                for (let opt in headers) {
                    if (headers.hasOwnProperty(opt)) {
                        client.setRequestHeader(opt, headers[opt]);
                    }
                }
            }

            if (data) {
                client.send(data);
            } else {
                client.send();
            }

            client.onload = function() {
                if (this.status >= 200 && this.status < 300) {
                    var response = this.response;
                    // Performs the function "resolve" when this.status is equal to 2xx
                    resolve((typeof response == 'string') ? JSON.parse(response) : response);
                } else {
                    // Performs the function "reject" when this.status is different than 2xx
                    reject(this.statusText);
                }
            };
            client.onerror = function() {
                reject(this.statusText);
            };
        });
    }

    // Adapter pattern
    return {
        'get': function(args, opts) {
            return ajax('GET', url, args, opts);
        },
        'post': function(args, opts) {
            return ajax('POST', url, args, opts);
        },
        'put': function(args, opts) {
            return ajax('PUT', url, args, opts);
        },
        'delete': function(args, opts) {
            return ajax('DELETE', url, args, opts);
        }
    };
}