/**
 * Created by timofeymelnik on 11/2/16.
 */

import AppDispatcher from "../dispatcher/AppDispatcher";
import LocationConstants from "../constants/LocationConstants";
import EventEmitter from "events";

const CHANGE_EVENT = 'change';

let _location;

function setLocation(location) {
    _location = location
}

function removeLocation() {
    _location = undefined;
}

class LocationStore extends EventEmitter {
    emitChange() {
        this.emit(CHANGE_EVENT)
    }

    addChangeListener(cb) {
        this.on(CHANGE_EVENT, cb)
    }

    removeChangeListener(cb) {
        this.removeListener(CHANGE_EVENT, cb)
    }

    getLocation() {
        return _location
    }
}

const locationStore = new LocationStore();

locationStore.dispatchToken = AppDispatcher.register(action => {
    switch(action.actionType) {
        case LocationConstants.SET_LOCATION:
            setLocation(action.location);
            locationStore.emitChange();
            break;
        case LocationConstants.REMOVE_LOCATION:
            removeLocation();
            locationStore.emitChange();
            break;
        default:
    }
});

export default locationStore