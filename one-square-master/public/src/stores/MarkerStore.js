/**
 * Created by timofeymelnik on 11/2/16.
 */

import AppDispatcher from "../dispatcher/AppDispatcher";
import MarkerConstants from "../constants/MarkerConstants";
import EventEmitter from "events";

const CHANGE_EVENT = 'change';

let _marker;

function setMarker(marker) {
    _marker = marker
}

function removeMarker() {
    _marker = undefined;
}

class MarkerStore extends EventEmitter {
    emitChange() {
        this.emit(CHANGE_EVENT)
    }

    addChangeListener(cb) {
        this.on(CHANGE_EVENT, cb)
    }

    removeChangeListener(cb) {
        this.removeListener(CHANGE_EVENT, cb)
    }

    getMarker() {
        return _marker
    }
}

const markerStore = new MarkerStore();

markerStore.dispatchToken = AppDispatcher.register(action => {
    switch(action.actionType) {
        case MarkerConstants.SET_MARKER:
            setMarker(action.marker);
            markerStore.emitChange();
            break;
        case MarkerConstants.REMOVE_MARKER:
            removeMarker();
            markerStore.emitChange();
            break;
        default:
    }
});

export default markerStore