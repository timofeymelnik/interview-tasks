/**
 * Created by timofeymelnik on 11/2/16.
 */

import AppDispatcher from "../dispatcher/AppDispatcher";
import MapTypeConstants from "../constants/MapTypeConstants";
import EventEmitter from "events";

const CHANGE_EVENT = 'change';

let _type;

function setType(type) {
    _type = type
}

class MapStore extends EventEmitter {
    emitChange() {
        this.emit(CHANGE_EVENT)
    }

    addChangeListener(cb) {
        this.on(CHANGE_EVENT, cb)
    }

    removeChangeListener(cb) {
        this.removeListener(CHANGE_EVENT, cb)
    }
}

const mapStore = new MapStore();

mapStore.dispatchToken = AppDispatcher.register(action => {
    switch(action.actionType) {
        case MapTypeConstants.SET_TYPE:
            setType(action.type);
            mapStore.emitChange();
            break;
        default:
    }
});

export default mapStore