/**
 * Created by timofeymelnik on 11/1/16.
 */

import AppDispatcher from "../dispatcher/AppDispatcher";
import AuthConstants from "../constants/AuthConstants";
import EventEmitter from "events";

const CHANGE_EVENT = 'change';

function setUser(userData, token) {
    if (!localStorage.getItem('jwt')) {
        localStorage.setItem('user_data', JSON.stringify(userData));
        localStorage.setItem('jwt', token);
    }
}

function removeUser() {
    localStorage.removeItem('user_data');
    localStorage.removeItem('jwt');
}

class AuthStore extends EventEmitter {
    emitChange() {
        this.emit(CHANGE_EVENT);
    }

    addChangeListener(cb) {
        this.on(CHANGE_EVENT, cb);
    }

    removeChangeListener(cb) {
        this.removeListener(CHANGE_EVENT, cb)
    }

    isAuthenticated() {
        return !!this.getJWT();
    }

    getUser() {
        return JSON.parse(localStorage.getItem('user_data'));
    }

    getJWT() {
        return localStorage.getItem('jwt');
    }
}

const authStore = new AuthStore();

authStore.dispatchToken = AppDispatcher.register(action => {
    switch (action.actionType) {
        case AuthConstants.AUTH_USER:
            setUser(action.credentials, action.token);
            authStore.emitChange();
            break;
        case AuthConstants.LOGOUT_USER:
            removeUser();
            authStore.emitChange();
            break;
        default:
    }
});

export default authStore