/**
 * Created by timofeymelnik on 10/31/16.
 */

import EventEmitter from 'events';
import AppDispatcher from "../dispatcher/AppDispatcher";
import UserConstants from "../constants/UserConstants";

const CHANGE_EVENT = 'change';

let _users = [];
let _user = [];

function setUsers(users) {
    _users = users
}

function setUser(user) {
    _user = user
}

class UserStore extends EventEmitter {
    emitChange() {
        this.emit(CHANGE_EVENT);
    }

    addChangeListener(cb) {
        this.on(CHANGE_EVENT, cb)
    }

    removeChangeListener(cb) {
        this.removeListener(CHANGE_EVENT, cb)
    }

    getUsers() {
        return _users;
    }

    getUser() {
        return _user;
    }
}

const userStore = new UserStore();

userStore.dispatchToken = AppDispatcher.register(action => {
    switch(action.actionType) {
        case UserConstants.GET_USER:
        case UserConstants.UPDATE_USER:
        case UserConstants.ADD_LOCATION:
            setUser(action.data);
            userStore.emitChange();
            break;
        case UserConstants.GET_USERS:
            setUsers(action.data);
            userStore.emitChange();
            break;
        case UserConstants.GET_USER_ERROR:
            console.log(action.message);
            userStore.emitChange();
            break;
        case UserConstants.GET_USERS_ERROR:
            console.log(action.message);
            userStore.emitChange();
            break;
        default:
    }
});

export default userStore;