/**
 * Created by timofeymelnik on 10/31/16.
 */

import {Dispatcher} from 'flux';

export default new Dispatcher();