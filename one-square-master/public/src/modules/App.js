/**
 * Created by timofeymelnik on 10/31/16.
 */

import React from "react";
import Header from "./Header";
import GMap from "./GMap";

class App extends React.Component {
    render() {
        return (
            <div className="grid-frame vertical">
                <Header />
                <div className="grid-frame">
                    <div className="grid-block block-6 wrap">
                        <GMap
                            center={{lat: 46.485722, lng: 30.743444}}
                            zoom={13}>
                        </GMap>
                    </div>
                    <div className="grid-block block-4 space-around content">
                        {this.props.children}
                    </div>
                </div>
            </div>
        )
    }
}

export default App