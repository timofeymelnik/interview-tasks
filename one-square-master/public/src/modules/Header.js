/**
 * Created by timofeymelnik on 10/31/16.
 */

import React from "react";
import NavLink from './NavLink';
import AuthStore from '../stores/AuthStore.js';

class Header extends React.Component {
    constructor() {
        super();

        this.state = {
            isAuthenticated: AuthStore.isAuthenticated()
        };

        this._onChange = this._onChange.bind(this);
    }

    componentWillMount() {
        AuthStore.addChangeListener(this._onChange)
    }

    componentWillUnmount() {
        AuthStore.removeChangeListener(this._onChange)
    }

    _onChange() {
        this.setState({
            isAuthenticated: AuthStore.isAuthenticated()
        })
    }

    render() {
        return (
            <div className="grid-block shrink wrap">
                <div className="grid-content collapse block-6">
                    <ul className="menu-bar">
                        <li><span>OneSquare</span></li>
                    </ul>
                </div>
                <div className="grid-content collapse block-4">
                    <ul className="menu-bar dark" role="nav">
                        <li><NavLink to="/users">Users</NavLink></li>
                        <li><NavLink to="/" onlyActiveOnIndex={true}>
                            { !this.state.isAuthenticated ? ('Auth') : ('Profile')}
                        </NavLink></li>
                        <li><NavLink to="/about">About</NavLink></li>
                    </ul>
                </div>
            </div>
        );
    }
}

export default Header