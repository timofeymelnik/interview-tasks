/**
 * Created by timofeymelnik on 10/31/16.
 */

import React from 'react'
import AuthStore from '../stores/AuthStore.js';
import LoginForm from './LoginForm';
import Profile from './Profile';

class Auth extends React.Component {
    constructor() {
        super();

        this.state = {
            isAuthenticated: AuthStore.isAuthenticated()
        };

        this._onChange = this._onChange.bind(this);
    }

    componentWillMount() {
        AuthStore.addChangeListener(this._onChange)
    }

    componentWillUnmount() {
        AuthStore.removeChangeListener(this._onChange)
    }

    _onChange() {
        this.setState({
            isAuthenticated: AuthStore.isAuthenticated()
        })
    }

    render() {
        return (
            <div className="block-9">
                { !this.state.isAuthenticated ? (
                    <LoginForm />
                ) : (
                    <Profile />
                )}
            </div>
        )
    }
}

export default Auth