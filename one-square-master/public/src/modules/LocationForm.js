/**
 * Created by timofeymelnik on 11/2/16.
 */

import React from "react";
import isEmpty from "../utils/isEmpty";
import LocationStore from "../stores/LocationStore";
import UserActions from "../actions/UsersActions";
import AuthStore from "../stores/AuthStore.js";

class LocationForm extends React.Component {
    constructor() {
        super();

        this.state = {
            location: {},
            name: '',
            type: ''
        };

        this.baseState = this.state;

        this._onChange = this._onChange.bind(this);
    }

    componentWillMount() {
        LocationStore.addChangeListener(this._onChange)
    }

    componentWillUnmount() {
        LocationStore.removeChangeListener(this._onChange)
    }

    _onChange() {
        this.setState(this.baseState);

        let location = LocationStore.getLocation();

        if (location) {
            this.setState({
                location: location,
                name: location.name,
                type: location.types.join(', ')
            });
        }
    }

    _handleSubmit(event) {
        event.preventDefault();

        let location = this.state.location.geometry.location;
        let name = this._name;
        let type = this._type;

        UserActions.addLocation(AuthStore.getUser().userName, {
            location: [location.lat(), location.lng()],
            name: name.value,
            type: type.value
        });

        this.setState(this.baseState)
    }

    _handleChange(key) {
        return function(event) {
            var state = {};
            state[key] = event.target.value;
            this.setState(state);
        }.bind(this);
    }

    _closeForm() {
        this.setState(this.baseState)
    }

    render() {
        let location;
        if (this.state.location) {
            location = this.state.location;
        }
        return (
            <div className="grid-block shrink">
                {!isEmpty(location) &&
                <form onSubmit={this._handleSubmit.bind(this)}>
                    <h3>Add location</h3>
                    <div>
                            <textarea placeholder="Place Name:" value={this.state.name}
                                      onChange={this._handleChange('name')} ref={name => this._name = name}/>
                    </div>
                    <div>
                        <input type="text" placeholder="Type:" value={this.state.type}
                               onChange={this._handleChange('type')} ref={type => this._type = type}/>
                    </div>
                    <div>
                        <button type="submit">Add</button>
                        <button type="button" onClick={this._closeForm.bind(this)}>Cancel</button>
                    </div>
                </form>}
            </div>
        );
    }
}

export default LocationForm