/**
 * Created by timofeymelnik on 11/2/16.
 */

import React from "react";
import MarkerActions from "../actions/MarkerActions";
import LocationActions from "../actions/LocationActions";

class Locations extends React.Component {
    render() {
        let locations;
        if (this.props.data) {
            locations = this._getLocations(this.props.data);
        }
        return (
            <ul>{locations}</ul>
        )
    }

    _getLocations(locations) {
        return locations.map(location => {
            return (
                <li className="list-link" key={location._id}>
                    <a href="#" onClick={this._handleClick(location.location)}>
                        <span>{location.name} <small>{location.type}</small></span>
                    </a>
                </li>
            );
        })
    }

    _handleClick(latLng) {
        return function(event) {
            event.preventDefault();

            MarkerActions.setMarker(latLng);
            LocationActions.removeLocation();
        }.bind(this);
    }
}
export default Locations