/**
 * Created by timofeymelnik on 11/1/16.
 */

import React from "react";
import Locations from "./Locations";
import isEmpty from "../utils/isEmpty";
import MarkerActions from "../actions/MarkerActions";
import MarkerStore from "../stores/MarkerStore";

class User extends React.Component {
    constructor() {
        super();

        this.state = {
            marker: MarkerStore.getMarker()
        };

        this._onMarkerChange = this._onMarkerChange.bind(this);
    }

    componentWillMount() {
        MarkerStore.addChangeListener(this._onMarkerChange)
    }

    componentWillUnmount() {
        MarkerStore.removeChangeListener(this._onMarkerChange)
    }

    _onMarkerChange() {
        this.setState({
            marker: MarkerStore.getMarker()
        })
    }

    render() {
        let locations;
        if (this.props.user) {
            locations = this.props.user.locations;
        }

        return (
            <div className="grid-frame vertical">
                <div className="grid-block shrink">
                    <h2>{this.props.user.userName}</h2>
                    {!isEmpty(this.state.marker) &&
                    <a href="#" onClick={this._backToSearch.bind(this)}>&larr; Back to search</a>
                    }
                </div>
                <div className="grid-block">
                    {!isEmpty(locations) && <Locations data={locations}/>}
                </div>
            </div>
        );
    }

    _backToSearch(event) {
        event.preventDefault();
        MarkerActions.removeMarker();
    }
}

export default User