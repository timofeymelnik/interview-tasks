/**
 * Created by timofeymelnik on 11/1/16.
 */

import React from "react";

class GenericNotFound extends React.Component {
    render() {
        return (
            <div>
                <h2>404</h2>
                <p>Not Found</p>
            </div>
        );
    }
}

export default GenericNotFound