/**
 * Created by timofeymelnik on 10/31/16.
 */
"use strict";

import React from 'react';
import UserActions from '../actions/UsersActions.js';
import UserStore from '../stores/UserStore';
import UserLink from './UserLink';

class UsersBox extends React.Component {
    constructor() {
        super();

        this.state = {
            users: []
        };

        this._onChange = this._onChange.bind(this);
    }

    render() {
        let usersList;
        if (this.state.users) {
            usersList = this._getUsersList();
        }
        return (
            <div className="grid-block block-9">
                <ul className="full-width">{usersList}</ul>
            </div>
        );
    }

    componentWillMount() {
        UserStore.addChangeListener(this._onChange);
    }

    componentDidMount() {
        UserActions.getUsers();
    }

    componentWillUnmount() {
        UserStore.removeChangeListener(this._onChange);
    }

    _onChange() {
        this.setState({
            users: UserStore.getUsers()
        });
    }

    _getUsersList() {
        return this.state.users.map(user => {
            return (
                <UserLink user={user.userName} key={user._id}/>
            )
        });
    }

}

export default UsersBox;