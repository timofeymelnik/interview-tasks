/**
 * Created by timofeymelnik on 11/1/16.
 */

import React from "react";
import AuthActions from '../actions/AuthActions';

class LoginForm extends React.Component {
    _handleSubmit(event) {
        event.preventDefault();

        let userName = this._userName;
        let password = this._password;

        AuthActions.auth({
            userName: userName.value,
            password: password.value
        }, btoa(`${userName.value}:${password.value}`));
    }

    render() {
        return (
            <form onSubmit={this._handleSubmit.bind(this)}>
                <h2>Login or Register</h2>
                <div>
                    <input type="text" placeholder="Username:" ref={userName => this._userName = userName}/>
                </div>
                <div>
                    <input type="password" placeholder="Password:" ref={password => this._password = password}/>
                </div>
                <div>
                    <button type="submit">Submit</button>
                </div>
            </form>
        );
    }
}

export default LoginForm