/**
 * Created by timofeymelnik on 10/31/16.
 */

import React from "react";
import MapActions from "../actions/MapTypeActions";
import MapTypeConstants from "../constants/MapTypeConstants";
import MarkerStore from "../stores/MarkerStore";
import LocationActions from "../actions/LocationActions";
import MarkerActions from "../actions/MarkerActions";

class GMap extends React.Component {
    constructor() {
        super();

        this.state = {
            type: 'restaurant'
        };

        MapActions.setType(this.state.type);

        this._handleChange = this._handleChange.bind(this);
        this._onChangeMarker = this._onChangeMarker.bind(this);
        this.isMapLoaded = false;
    }

    _getTypesOptions() {
        var options = MapTypeConstants.OPTIONS;
        return Object.keys(options).map(optionKey => {
            return (
                <option value={optionKey} key={optionKey}>{options[optionKey]}</option>
            );
        })
    }

    _onMarkerAdd(marker) {
        this._setMapOnAll(null);

        this.map.setCenter(this._mapCenter(marker[0], marker[1]));

        this.markers = [this._addMarker({
            lat: marker[0],
            lng: marker[1]
        })];

        this._setMapOnAll(this.map);
    }

    _onChangeMarker() {
        let marker = MarkerStore.getMarker();

        if (marker) {
            this._onMarkerAdd(marker);
        } else {
            this._updateMap()
        }
    }

    render() {
        let options = this._getTypesOptions();
        return (
            <div className="grid-frame vertical">
                <div className="grid-block type-selector">
                    <select className="block-3" value={this.state.type} onChange={this._handleChange}>
                        {options}
                    </select>
                </div>
                <div className="GMap" ref="mapCanvas"></div>
            </div>
        )
    }

    componentWillMount() {
        MarkerStore.addChangeListener(this._onChangeMarker)
    }

    componentDidMount() {
        this.map = this._createMap();
        this.infoWindow = new google.maps.InfoWindow();
        this.service = new google.maps.places.PlacesService(this.map);
        this.markers = [];

        this.map.addListener('tilesloaded', () => {
            if (!this.isMapLoaded) {
                this._performSearch();
                this.isMapLoaded = true;
            }
        });
    }

    componentWillUnmount() {
        MarkerStore.removeChangeListener(this._onChangeMarker)
    }

    _handleChange(event) {
        let type = event.target.value;

        this.setState({type}, () => {
            MapActions.setType(type);

            if (MarkerStore.getMarker()) {
                MarkerActions.removeMarker();
            } else {
                this._updateMap();
            }
        });
    }

    _updateMap() {
        this._setMapOnAll(null);
        this.markers = [];

        this._performSearch();
    }

    _setMapOnAll(map) {
        this.markers.forEach(marker => {
            marker.setMap(map)
        })
    }

    _createMap() {
        return new google.maps.Map(this.refs.mapCanvas, {
            zoom: this.props.zoom,
            center: this._mapCenter(this.props.center.lat, this.props.center.lng),
            disableDefaultUI: true,
            styles: [{
                stylers: [{visibility: 'simplified'}]
            }, {
                elementType: 'labels',
                stylers: [{visibility: 'off'}]
            }]
        })
    }

    _mapCenter(lat, lng) {
        return new google.maps.LatLng(lat, lng)
    }

    _performSearch() {
        this.service.nearbySearch({
            bounds: this.map.getBounds(),
            type: this.state.type
        }, (results, status) => {
            this._callback(results, status)
        });
    }

    _callback(results, status) {
        if (status !== google.maps.places.PlacesServiceStatus.OK) {
            console.error(status);
            return;
        }
        for (let i = 0, result; result = results[i]; i++) {
            let marker = this._addMarker(result.geometry.location, {
                icon: {
                    url: 'http://maps.gstatic.com/mapfiles/circle.png',
                    anchor: new google.maps.Point(10, 10),
                    scaledSize: new google.maps.Size(10, 17)
                }
            });
            this.markers.push(marker);

            google.maps.event.addListener(marker, 'click', () => {
                this.service.getDetails(result, (result, status) => {
                    if (status !== google.maps.places.PlacesServiceStatus.OK) {
                        console.error(status);
                        return;
                    }
                    this.infoWindow.setContent(result.name);
                    this.infoWindow.open(this.map, marker);
                    LocationActions.setLocation(result);
                });
            });
        }
        this._setMapOnAll(this.map);
    }

    _addMarker(place, icon) {
        return new google.maps.Marker(Object.assign({
            position: place,
        }, icon || {}));
    }
}

export default GMap