/**
 * Created by timofeymelnik on 10/31/16.
 */

import React from "react";
import UserActions from '../actions/UsersActions.js';
import UserStore from '../stores/UserStore.js';
import User from './User';

class UserDetails extends React.Component {
    constructor() {
        super();

        this.state = {
            user: {}
        };

        this._onChange = this._onChange.bind(this);
    }

    componentWillMount() {
        UserStore.addChangeListener(this._onChange);
    }

    componentDidMount() {
        UserActions.getUser(this.props.params['userId']);
    }

    componentWillUnmount() {
        UserStore.removeChangeListener(this._onChange);
    }

    componentWillReceiveProps(nextProp) {
        this.setState({
            user: UserActions.getUser(nextProp.params['userId'])
        })
    }

    _onChange() {
        this.setState({
            user: UserStore.getUser(this.props.params['userId'])
        });
    }

    render() {
        let user;
        if (this.state.user) {
            user = this.state.user;
        }
        return (
            <div className="grid-block block-9">
                {user && <User user={user} />}
            </div>
        );
    }
}

export default UserDetails