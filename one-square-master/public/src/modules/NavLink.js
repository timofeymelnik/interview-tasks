/**
 * Created by timofeymelnik on 10/31/16.
 */

import React from 'react'
import {Link} from 'react-router'

class NavLink extends React.Component {
    render() {
        return <Link {...this.props} activeClassName="is-active"/>
    }
}

export default NavLink