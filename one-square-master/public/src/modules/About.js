/**
 * Created by timofeymelnik on 10/31/16.
 */

import React from 'react'

class About extends React.Component {
    render() {
        return (
            <div className="block-9">
                <h2>About</h2>
                <p>It is test project, you can read more at project description on <a href="https://github.com/timofeymelnik/one-square">GitHub</a></p>
            </div>
        );
    }
}

export default About