/**
 * Created by timofeymelnik on 10/31/16.
 */

import React from "react";
import NavLink from "./NavLink";
import AuthStore from "../stores/AuthStore.js";


class UserLink extends React.Component {
    render() {
        let user = this.props.user;
        var currentUser = AuthStore.getUser();
        return (
            <li className="list-link">
                <NavLink to={(currentUser && currentUser.userName == user.userName) ? `/users/${user}` : '/'}>{user}</NavLink>
            </li>
        )
    }
}

export default UserLink;