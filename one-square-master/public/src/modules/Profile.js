/**
 * Created by timofeymelnik on 11/1/16.
 */

import React from "react";
import UserStore from "../stores/UserStore";
import AuthActions from "../actions/AuthActions";
import UserActions from "../actions/UsersActions";
import LocationForm from "./LocationForm";
import User from "./User";

class Profile extends React.Component {
    constructor() {
        super();

        this.state = {
            user: {}
        };

        this._onChange = this._onChange.bind(this);
    }

    _onChange() {
        this.setState({
            user: UserStore.getUser()
        })
    }

    componentWillMount() {
        UserStore.addChangeListener(this._onChange)
    }

    componentDidMount() {
        UserActions.getUser('me');
    }

    componentWillUnmount() {
        UserStore.removeChangeListener(this._onChange)
    }

    _logOut() {
        AuthActions.logOut();
    }

    render() {
        return (
            <div className="grid-frame vertical">
                <div className="grid-block">
                    <div className="grid-block block-7">
                        <h2>Profile</h2>
                    </div>
                    <div className="grid-block block-3">
                        <button type="button" onClick={this._logOut.bind(this)}>LogOut</button>
                    </div>
                </div>
                <div className="grid-frame vertical profile">
                    <User user={this.state.user}/>
                    <LocationForm />
                </div>
            </div>
        );
    }
}

export default Profile