/**
 * Created by timofeymelnik on 10/31/16.
 */

"use strict";

import React from "react";
import ReactDOM from "react-dom";
import {Router, Route, hashHistory, IndexRoute} from 'react-router';

import './less/main.less';

import App from './modules/App';
import About from './modules/About';
import Auth from './modules/Auth';
import UsersBox from './modules/UsersBox';
import UserDetails from './modules/UserDetails';
import GenericNotFound from "./modules/GenericNotFound";

ReactDOM.render(
    (<Router history={hashHistory}>
        <Route path="/" component={App}>
            <IndexRoute component={Auth}/>
            <Route path="/about" component={About}/>
            <Route path="/users" component={UsersBox}/>
            <Route path="/users/:userId" component={UserDetails}/>
            <Route path="*" component={GenericNotFound}/>
        </Route>
    </Router>), document.getElementById('app'));