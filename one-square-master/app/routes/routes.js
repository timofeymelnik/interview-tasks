/**
 * Created by timofeymelnik on 10/30/16.
 * Describe app routes
 */

var express = require('express');

var AuthController = require('../controllers/auth');
var UserController = require('../controllers/user');

var apiRoutes = express.Router();

function routes(app) {
    apiRoutes.get('/users', (req, res) => UserController.users(req, res));
    apiRoutes.get('/users/me', (req, res) => UserController.currentUser(req, res, app.get('superSecret')));
    apiRoutes.get('/users/:userName', (req, res) => UserController.user(req, res));
    apiRoutes.put('/users/:userName', (req, res) => UserController.updateUser(req, res));
    apiRoutes.post('/users/:userName/locations', (req, res) => UserController.pushLocation(req, res));

    apiRoutes.post('/auth', (req, res) => AuthController.auth(req, res, app));

    // enable check token on each request
    // apiRoutes.use((req, res, next) => AuthController.tokenMiddleWare(req, res, next, app));

    app.use('/api/v1', apiRoutes);
}

module.exports = routes;