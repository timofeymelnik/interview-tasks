let db = 'mongodb://localhost/oneSquare';
let secret = 'superSecret';
let port = 3000;

module.exports = {
    secret,
    db,
    port
};
