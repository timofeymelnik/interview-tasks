const { db } = require('./config');
const mongoose = require('mongoose');

module.exports = {
    connect: () => {
        mongoose.Promise = Promise;
        mongoose.connect(db, {
            useMongoClient: true,
        });
        return mongoose;
    },
    mongoose
};