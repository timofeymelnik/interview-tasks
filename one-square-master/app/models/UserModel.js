/**
 * Created by timofeymelnik on 10/30/16.
 * Describe User mongodb schema
 * Export User model
 */
var bCrypt = require('bcrypt');
const {mongoose} = require('../db');
var Schema = mongoose.Schema;

var UserSchema = new Schema({
    userName: {type: String, trim: true, required: true},
    password: {type: String, required: true},
    created: {type: Date, default: Date.now},
    locations: [{
        location: {type: [Number]},
        name: {type: String},
        type: {type: String}
    }]
}, {
    collection: 'user',
    usePushEach: true
});

UserSchema.pre('save', function(next) {
    var user = this;

    // only hash the password if it has been modified (or is new)
    if (!user.isModified('password')) return next();

    // generate a salt
    bCrypt.genSalt(10, function(err, salt) { //salt_work_factor
        if (err) return next(err);

        // hash the password using our new salt
        bCrypt.hash(user.password, salt, function(err, hash) {
            if (err) return next(err);

            // override the cleartext password with the hashed one
            user.password = hash;
            next();
        });
    });
});

UserSchema.methods = {
    comparePassword: function(candidatePassword, cb) {
        bCrypt.compare(candidatePassword, this.password, function(err, isMatch) {
            if (err) return cb(err);
            cb(null, isMatch);
        });
    },

    addLocation: function(location) {
        this.locations.push(location);

        return this.save();
    },

    removeLocation: function(commentId) {
        const index = this.locations
            .map(location => location.id)
            .indexOf(commentId);

        if (~index) this.locations.splice(index, 1);
        else throw new Error('Location not found');

        return this.save();
    }
};

UserSchema.index({userName: 1}, {unique: true});

module.exports = mongoose.model('User', UserSchema);