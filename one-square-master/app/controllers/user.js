/**
 * Created by timofeymelnik on 11/1/16.
 */

const jwt = require('jsonwebtoken');
const User = require('../models/UserModel');

/**
 * Parse payload and return object
 * @param headers
 * @param headers.authorization
 * @returns {*}
 */
function parsePayload(headers) {
    const token = headers['x-access-token'];

    if (!token) return false;
    const [header, payload, sign] = token.split('.');

    if (payload.length !== 3) return false;
    return {token, header, payload, sign};
}

////////

module.exports = {
    users: async (req, res) => {
        const users = await User.find({});
        res.send(users || []);
    },
    currentUser: async ({headers}, res, secret) => {
        const {token} = parsePayload(headers);
        const {userName} = jwt.verify(token, secret);

        if (!userName) {
            return res.send(403, {message: 'Token must be provided'})
        }

        const user = await User.findOne({userName});
        if (!user) {
            return res.send(403, {message: 'UserLink not found'})
        }
        res.json(user);
    },
    user: async ({params: {userName}}, res) => {
        const user = await User.findOne({userName});
        if (!user) return res.send(400, {message: 'User not found'});

        res.json(user);
    },
    updateUser: async ({body, params: {userName}}, res) => {
        const user = await User.findOneAndUpdate({userName}, body, {upsert: true});
        if (!user) return res.send(500, {message: 'Something went wrong'});
        res.json(user);
    },
    pushLocation: async ({body, params: {userName}}, res) => {
        const user = await User.findOne({userName});
        if (!user) return res.send(400, {message: 'User not found'});

        user.addLocation(body);
        res.send(user);
    }
};