/**
 * Created by timofeymelnik on 11/1/16.
 */

var jwt = require('jsonwebtoken');
var User = require('../models/UserModel');

module.exports = {
    auth: function(req, res, app) {
        if (!req.headers['x-access-token']) res.json({message: 'Authorization data must be provided'});

        // find the user by parsed userName from headers
        var auth = new Buffer(req.headers['x-access-token'], 'base64').toString('ascii').split(':');

        User.findOne({
            userName: auth[0] //userName
        }, function(err, user) {
            if (err) {
                res.send(err);
            }
            if (user) {
                this.login(user, auth, res, app)
            } else {
                this.register(req.body, res, app)
            }
        }.bind(this))
    },

    login: function(user, auth, res, app) {
        // check if password matches
        user.comparePassword(auth[1], function(err, isMatch) { //password
            if (isMatch && !err) {
                // if user is found and password is right
                // create a token
                // return the information including token as JSON
                res.json({
                    token: jwt.sign({userName: auth[0]}, app.get('superSecret'), {
                        expiresIn: "24h" // expires in 24 hours
                    })
                });
            } else {
                res.status(401).json({message: 'Wrong password'});
            }
        });
    },
    register: function(user, res, app) {
        if (user.userName === 'me') {
            res.json({message: 'Reserved username'})
        }
        new User(user).save(function(err) {
            if (err) {
                res.send(err);
                return;
            }

            // create a token
            // return the information including token as JSON
            res.json({
                token: jwt.sign({userName: user.userName}, app.get('superSecret'), {
                    expiresIn: "24h" // expires in 24 hours
                })
            });
        });
    },
    // Uncomment to set token in every request
    // tokenMiddleWare: function(req, res, next, app) {
    //
    //     // check header for token
    //     var token = req.headers['x-access-token'];
    //
    //     // decode token
    //     if (token) {
    //         // verifies secret and checks exp
    //         jwt.verify(token, app.get('superSecret'), function(err, decoded) {
    //             if (err) {
    //                 return res.status(403).json({message: 'Failed to authenticate token.'});
    //             } else {
    //                 // if everything is good, save to request for use in other routes
    //                 req.decoded = decoded;
    //                 next();
    //             }
    //         });
    //     } else {
    //         // if there is no token
    //         // return an error
    //         return res.status(403).send({
    //             success: false,
    //             message: 'No token provided.'
    //         });
    //
    //     }
    // }
};