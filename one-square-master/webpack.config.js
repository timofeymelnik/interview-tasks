/**
 * Created by timofeymelnik on 10/31/16.
 */

'use strict';

var webpack = require('webpack');
var path = require('path');
var srcPath = path.join(__dirname, 'public/src');
var ExtractTextPlugin = require('extract-text-webpack-plugin');

module.exports = {
    target: 'web',
    cache: true,
    entry: {
        module: path.join(srcPath, 'app.js'),
        common: ['react', 'react-router']
    },
    output: {
        path: path.join(__dirname, 'public/app'),
        filename: 'app.js'
    },
    module: {
        loaders: [
            {
                test: /\.jsx?$/, exclude: /node_modules/, loader: 'babel-loader', query: {presets: ['es2015', 'react']}
            }, {
                test: /\.less?$/, loader: ExtractTextPlugin.extract("style-loader", "css-loader!less-loader")
            }
        ]
    },
    plugins: [
        new ExtractTextPlugin("styles.css")
    ],
    devServer: {
        contentBase: './public',
        historyApiFallback: true
    },
    debug: true
};