/**
 * Created by timofeymelnik on 10/30/16.
 * Server entry point
 */

//Dependencies
var express = require('express');
var bodyParser = require('body-parser');
var config = require('./app/config');
var methodOverride = require('method-override');
const db = require('./app/db');
var app = express();
var port = process.env.PORT || config.port;

db.connect();

app.set('superSecret', config.secret);

app.use(express.static(__dirname + '/public'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extend: true}));
app.use(bodyParser.text());
app.use(bodyParser.json({type: 'application/vnd.api+json'}));
app.use(methodOverride());

require('./app/routes/routes')(app);

// Error Handling
app.use(function(req, res) {
    res.status(res.status || 500);
});

app.listen(port);

console.log('App running on port: ' + port);