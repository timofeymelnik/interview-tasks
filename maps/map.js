(function() {
	var defaults = {
		mapItem: '#mapItem'
	};

	//Объектное создание
	window.maps = function (selector, params) {
		if (arguments.length == 0)
			throw 'Error: не указан селектор';
		
		return new MapsSection( $(arguments[0]), params );
	};

	//jQuery плагин
	jQuery.fn.maps = function (params) {
		return new MapsSection( $(this), params );
	};

	//Создание объекта
	function MapsSection ($el, params) {
		var self = this;
		self.$el = $el;
		self.el = $el[0];
		self.el.self = self;

		if (params) {
			$.extend(defaults, params);
		}
		
		self.initialize();

	};

	MapsSection.fn = MapsSection.prototype;

	//Инициализация
	MapsSection.fn.initialize = function () {
		var self = this;
		self.itemTpl = $(defaults.mapItem).html();
		_.bindAll(this);
		this.mapCounter = 0;
		self.maps = [];
		self.poligons = [];

		self.$el.on('click', '#addMap', self.addMap);
		self.$el.on('click', '.delMap', self.deleteMap);
		self.$el.on('click', '.addPolygon', self.addPolygon);
		self.$el.on('click', '.accept', self.acceptPolygon);
		self.$el.on('click', '.cancel', self.cancelPolygon);
		self.$el.on('click', '.clear', self.clearPolygon);

		if (defaults.data)
			self.import(defaults.data);
	};

	//Добавление карты
	MapsSection.fn.addMap = function (e) {
		e.preventDefault();
		var $this = $(e.currentTarget);
		
		this.$el.append( _.template(this.itemTpl, { id: this.mapCounter++, price: null, value: null, coords: null } ) );
		var mapId = this.$el.find('.map').eq(-1).attr('id');
		
		var idx = this.maps.push( new ymaps.Map( mapId, { center: [56.317655, 43.994362], zoom: 15 } ) ) - 1;
		this.maps[idx].controls.add('zoomControl').add('typeSelector').add('mapTools');
		
		this.maps[idx].status = 'clear';
		this.setControls(idx);
	};

	MapsSection.fn.setControls = function (idx) {
		var $wrap = this.$el.find('.map-wrap').eq(idx);
		
		switch (this.maps[idx].status) {
			case 'clear':
				$wrap.find('.clear, .cancel, .accept').addClass('hide');
				$wrap.find('.addPolygon').removeClass('hide');
				break;

			case 'edit':
				$wrap.find('.clear, .addPolygon').addClass('hide');
				$wrap.find('.cancel, .accept').removeClass('hide');
				break;

			case 'done':
				$wrap.find('.cancel, .addPolygon, .accept').addClass('hide');
				$wrap.find('.clear').removeClass('hide');
				break;

		}

	};

	//Удаление карты
	MapsSection.fn.deleteMap = function (e) {
		e.preventDefault();
		var $this = $(e.currentTarget);
		var idx = $this.closest('.map-wrap').index() - 1;
		this.maps.splice(idx, 1);
		this.poligons.splice(idx, 1);
		$this.closest('.map-wrap').remove();
		
	};

	//Запуск на редактирвание полигона
	MapsSection.fn.addPolygon = function (e) {
		e.preventDefault();
		var $this = $(e.currentTarget);

		var idx = $this.closest('.map-wrap').index() - 1;

		this.poligons[idx] = new ymaps.Polygon([[]]);
		this.maps[idx].geoObjects.add(this.poligons[idx]);
		this.poligons[idx].editor.startDrawing();
		this.poligons[idx].events.add('editorstatechange', this.onStopDrawing);

		this.maps[idx].status = 'edit';
		this.setControls(idx);
		
	};

	MapsSection.fn.onStopDrawing = function (e) {
 		var idx = _.indexOf(this.poligons, e.get('target') ); //$(.getMap().container.getElement()).closest('.map-wrap').index() - 1;
 		
 		
 		if (!(e.get('target').editor.state.get('editing') == true && e.get('target').editor.state.get('drawing') == false) ) {
 			return;
 		}
 		
		this.poligons[idx].editor.stopEditing();
		var coords = this.poligons[idx].geometry.getCoordinates();

		
		if (coords[0].length < 4) {
			this.$el.find('.map-wrap').eq(idx).find('.cancel').click();	
		} else {
			this.$el.find('.map-wrap').eq(idx).find('.hiddenInput').val( JSON.stringify( this.poligons[idx].geometry.getCoordinates() ) );

			this.maps[idx].status = 'done';
			this.setControls(idx);
		
		}
	};

	//Окончание редактирования полигона
	MapsSection.fn.acceptPolygon = function (e) {
		e.preventDefault();
		var $this = $(e.currentTarget);

		var idx = $this.closest('.map-wrap').index() - 1;

		this.poligons[idx].editor.stopEditing();
		var coords = this.poligons[idx].geometry.getCoordinates();
		
		if (coords[0].length < 4) {
			this.cancelPolygon(e);
		} else {
			$this.closest('.map-wrap').find('.hiddenInput').val( JSON.stringify( this.poligons[idx].geometry.getCoordinates() ) );

			this.maps[idx].status = 'done';
			this.setControls(idx);
		
		}
		
	};

	//Отмена редактирования полигона
	MapsSection.fn.cancelPolygon = function (e) {
		e.preventDefault();
		var $this = $(e.currentTarget);

		var idx = $this.closest('.map-wrap').index() - 1;

		this.poligons[idx].editor.stopEditing();
		this.maps[idx].geoObjects.remove(this.poligons[idx]);

		this.maps[idx].status = 'clear';
		this.setControls(idx);
	};

	//Очистка карты
	MapsSection.fn.clearPolygon = function (e) {
		e.preventDefault();
		var $this = $(e.currentTarget);

		var idx = $this.closest('.map-wrap').index() - 1;

		this.maps[idx].geoObjects.remove(this.poligons[idx]);
		$this.closest('.map-wrap').find('.hiddenInput').val('');

		this.maps[idx].status = 'clear';
		this.setControls(idx);
	
	};


	MapsSection.fn.export = function () {
		var self = this;
		var data = [];

		this.$el.find('.map-wrap').each(function() {
			var $this = $(this);
			data.push ( {
				select: $this.find('select').val(),
				value: $this.find('.valInput').val(),
				price: $this.find('.priceInput').val(),
				coords: $this.find('.hiddenInput').val()
			});
		});

		return data;
	
	};

	MapsSection.fn.import = function (_data) {
		var self = this;
		if ( typeof(_data) == 'string' )
			_data = JSON.parse(_data);

		_.each(_data, function (item, index, list) {
			self.$el.append( _.template(self.itemTpl, { 
				id: self.mapCounter++, 
				price: item.price, 
				value: item.value, 
				coords: item.coords, 
				select: item.select 
			} ) );
			var mapId = self.$el.find('.map').eq(-1).attr('id');
			
			var idx = self.maps.push( new ymaps.Map( mapId, { center: [56.317655, 43.994362], zoom: 15 } ) ) - 1;
			self.maps[idx].controls.add('zoomControl').add('typeSelector').add('mapTools');
			
			self.poligons[idx] = new ymaps.Polygon( JSON.parse(item.coords) );
			self.maps[idx].geoObjects.add(self.poligons[idx]);

			self.maps[idx].status = 'done';
			self.setControls(idx);
		});
	
	};

	

})();