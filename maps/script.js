(function() {

	var select = 'name-{{select}}';
	var inputNameVal = 'name-{{inputNameVal}}';
	var inputNamePrice = 'name-{{inputNamePrice}}';
	var hiddenInputName = 'name-{{hiddenInputName}}';

	var id = $('section').find('.map').last().data('id');
	var mapId = 'map-' + id;
	var mapWrap = 'map-wrap-' + id;

	$(document).on('click', '#addPolygon', addPolygon);
	$(document).on('click', '#endPolygon', endPolygon);
	$(document).on('click', '#addMap', addMap);
	$(document).on('click', '#delMap', delMap);

	$('.map').each(function () {
		var fields = $("input").serializeArray();
		var temp = new Array (fields);

		data.push(temp);
	});

	function parser (_data) {
		for(var i = 0; i < _data.length; i++) {
			addMap();
			var map = _data[i];
		}
	}
	
	function addMap() {  
  	var id = (id === null) ? '1' : id++;

		renderMap(id);

		myMap = new ymaps.Map(mapId, {center: [56.317655,43.994362], zoom: 15});
		myMap.controls.add('zoomControl').add('typeSelector').add('mapTools');
	};

	function renderMap (id) {
		var mapId = 'map-' + id;
		var mapWrap = 'map-wrap-' + id;
		var hide = $('.hide');
		var form = $('#form');
		$('<div id="' + mapWrap + '"></div>').appendTo('section');
		$('<div id="' + mapId + '" data-id="' + id + '" class="map"></div>').appendTo('#' + mapWrap);
		hide.find('.btn').clone().prependTo('#' + mapWrap);
		hide.find('#form').clone().appendTo('#' + mapWrap);
		form.find('select.inputName').attr('name', select.replace('\{\{select\}\}', 'select' + id));
		form.find('input#inputNameVal').attr('name', inputNameVal.replace('\{\{inputNameVal\}\}', 'inputNameVal' + id));
		form.find('input#inputNamePrice').attr('name', inputNamePrice.replace('\{\{inputNamePrice\}\}', 'inputNamePrice' + id));
	}

	function delMap (e) {
		$(e.currentTarget).parent().html('');
	};

	function addPolygon (e) {
		polygon = new ymaps.Polygon([[]]);
		myMap.geoObjects.add(polygon);
		polygon.editor.startDrawing();
		$('.btn#endPolygon').last().attr('disabled', false);
		$(e.currentTarget).attr('disabled', true);
	};

	function endPolygon (e) {
		polygon.editor.stopEditing();
		printGeometry(polygon.geometry.getCoordinates());
		$(e.currentTarget).attr('disabled', true);
	};

	function printGeometry (coords) {
		var last = $('section').find('.map').last().parent();
		$('<input type="text" />')
			.attr('value', coords)
			.attr('name', hiddenInputName.replace('\{\{hiddenInputName\}\}', 'hiddenInputName' + id))
			.appendTo(last);
	};

})();